package com.example.nima;

import android.app.Application;

import com.backtory.java.internal.BacktoryClient;
import com.backtory.java.internal.KeyConfiguration;
import com.backtory.java.internal.LogLevel;

/**
 * Created by Nima on 12/6/2017.
 */
public class MainApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        BacktoryClient.setDebugMode(BuildConfig.DEBUG);
        BacktoryClient.setLogLevel(LogLevel.Debug);
        // Initializing backtory
        BacktoryClient.init(KeyConfiguration.newBuilder().
                // Enabling User Services
                        setAuthKeys("5a285044e4b027741c2205cc",
                        "5a285044e4b0a3ac335b4f7f").
                // Finalizing sdk
                        build(), this);
    }
}
