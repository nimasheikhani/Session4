package com.example.nima;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.backtory.java.HttpStatusCode;
import com.backtory.java.internal.BacktoryCallBack;
import com.backtory.java.internal.BacktoryResponse;
import com.backtory.java.internal.BacktoryUser;
import com.backtory.java.model.LoginResponse;
import com.example.nima.Model.TokenModel;
import com.example.nima.Utils.PublicMethods;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;
import com.orhanobut.hawk.Hawk;

import java.io.UnsupportedEncodingException;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;

public class LoginActivity extends BaseActivity implements View.OnClickListener {

    EditText txtLoginEmail;
    EditText txtLoginPassword;
    Button btnLogin;
    String TAG = "backtoryLogin_";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        BindView();
        GetExtraDataForIntend();
    }

    private void BindView() {
        txtLoginEmail = (EditText) findViewById(R.id.txtLoginEmail);
        txtLoginPassword = (EditText) findViewById(R.id.txtLoginPassword);
        findViewById(R.id.btnLogin).setOnClickListener(this);
    }

    public void Login() {
        final String username = txtLoginEmail.getText().toString();
        String password = txtLoginPassword.getText().toString();

        AsyncHttpClient client = new AsyncHttpClient();


        client.addHeader("X-Backtory-Authentication-Id", "5a285044e4b027741c2205cc");
        client.addHeader("X-Backtory-Authentication-Key", "762e3be0f88f47d6937fcc9a");
        client.addHeader("Content-Type", "application/json");
        client.addHeader("token", (String) Hawk.get("token"));

        StringEntity entity = null;
        try {

            entity = new StringEntity("" +
                    "{\n" +
                    "        \"username\":\"" + username + "\",\n" +
                    "        \"password\":\"" + password + "\",\n" +
                    "    }" +
                    "" +
                    "" +
                    "");

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        client.post(mContext, "https://api.backtory.com/auth/login", entity, "application/json", new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Log.d(TAG, "onFailure: " + throwable);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                Log.d(TAG, "onSuccess: " + responseString);
                Gson gson = new Gson();
                TokenModel token = gson.fromJson(responseString, TokenModel.class);
                Hawk.put("loginToken", token.getAccessToken());
                PublicMethods.ShowToast(mContext,"Login !!!");
            }
        });

    }

    void Login2() {

        AsyncHttpClient client = new AsyncHttpClient();

        client.addHeader("X-Backtory-Authentication-Id", "5a285044e4b027741c2205cc");
        client.addHeader("X-Backtory-Authentication-Key", "762e3be0f88f47d6937fcc9a");
        client.addHeader("Content-Type", "application/json");
        client.addHeader("token", (String) Hawk.get("token"));
        StringEntity entity = null;
        try {

            entity = new StringEntity("" +
                    "{\n" +
                    "        \"username\":\"arian\",\n" +
                    "        \"password\":\"ar1393nima\",\n" +
                    "    }" +
                    "" +
                    "" +
                    "");

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        client.post(mContext, "https://api.backtory.com/auth/login", entity, "application/json", new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Log.d(TAG, "onFailure: " + throwable);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                Log.d(TAG, "onSuccess: " + responseString);
                Gson gson = new Gson();
                TokenModel token = gson.fromJson(responseString, TokenModel.class);
                Hawk.put("token", token.getAccessToken());
            }
        });

    }

    private void GetExtraDataForIntend() {
        try {
            Intent intent = getIntent();

            String Email = intent.getStringExtra("Email");
            Log.d(TAG, "get email: " + Email);

            if (Email != null) {
                txtLoginEmail.setText(Email);
            }
        } catch (Exception ex) {
            PublicMethods.ShowToast(mContext, ex.getMessage());
        }
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btnLogin)
        {
            Login();
        }
    }
}
