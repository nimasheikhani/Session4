package com.example.nima;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.backtory.java.HttpStatusCode;
import com.backtory.java.internal.BacktoryCallBack;
import com.backtory.java.internal.BacktoryResponse;
import com.backtory.java.internal.BacktoryUser;
import com.example.nima.Utils.PublicMethods;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;
import com.orhanobut.hawk.Hawk;

import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import okio.BufferedSink;

import static android.provider.AlarmClock.EXTRA_MESSAGE;

public class RegisterActivity extends BaseActivity implements View.OnClickListener {

    EditText txtName;
    EditText txtLastName;
    EditText txtEmail;
    EditText txtPassword;
    Button btnSignup;
    String TAG = "backtory_";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        BindView();
    }

    private void BindView() {
        txtName = (EditText) findViewById(R.id.txtName);
        txtLastName = (EditText) findViewById(R.id.txtLastName);
        txtEmail = (EditText) findViewById(R.id.txtEmail);
        txtPassword = (EditText) findViewById(R.id.txtPassword);
        findViewById(R.id.btnSignup).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btnSignup) {
            try {
                CallRegistrationAPI();
            } catch (Exception ex) {
                PublicMethods.ShowToast(mContext, ex.getMessage());
            }
        }
    }

    private void CallRegistrationAPI() {
        AsyncHttpClient client = new AsyncHttpClient();

        client.addHeader("X-Backtory-Authentication-Id", "5a285044e4b027741c2205cc");
        client.addHeader("Content-Type", "application/json");
        StringEntity entity = null;
        try {

            entity = new StringEntity("" +
                    "{\n" +
                    "        \"firstName\":\"" + txtName.getText() + "\",\n" +
                    "        \"lastName\":\"" + txtLastName.getText() + "\",\n" +
                    "        \"username\":\"" + txtEmail.getText() + "\",\n" +
                    "        \"password\":\"" + txtPassword.getText() + "\",\n" +
                    "        \"email\":\"" + txtEmail.getText() + "\"\n" +
                    "    }" +
                    "");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        client.post(mContext, "https://api.backtory.com/auth/users", entity, "application/json", new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Log.d(TAG, "onFailure: " + throwable);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                Log.d(TAG, "onSuccess: " + responseString);
                try {
                    JSONObject allObj = new JSONObject(responseString);
                    String token = allObj.getString("userId");
                    Hawk.put("token", token);
                } catch (Exception ex) {

                }
                RedirectToLogin();
            }
        });
    }

    private void RedirectToLogin() {
        Intent intent = new Intent(this, LoginActivity.class);
        String message = txtEmail.getText().toString();
        intent.putExtra("Email", message);
        startActivity(intent);
    }

    private void ClearTexts() {
        txtName.setText("");
        txtLastName.setText("");
        txtEmail.setText("");
        txtPassword.setText("");
    }
}
